package org.agil.persistencia;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.agil.Persona;

public class RepositorioEnMemoria implements Repositorio{
	HashSet<Persona> personas;
	
	public void anyadirPersona(Persona persona){
		if(personas == null)
			personas = new HashSet<Persona>();
		
		personas.add(persona);
	}
	
	@Override
	public void guardar(Persona persona) {
	}
}
