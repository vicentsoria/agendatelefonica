package org.agil.persistencia;

import org.agil.Persona;

public interface Repositorio {
	void anyadirPersona(Persona persona);
	void guardar(Persona persona);

}
