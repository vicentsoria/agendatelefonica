package org.agil;

import org.agil.persistencia.Repositorio;

public class Persona{// implements Comparable{
	private Repositorio repositorio;
	private String telefono;
	private String identificador;
	
	public Persona(String id){
		this.identificador = id;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public boolean persistir(){
		repositorio.guardar(this);
		
		return true;
	}

	/*public int compareTo(Persona persona) {
		return this.identificador.compareTo(persona.identificador);
	}*/


}
