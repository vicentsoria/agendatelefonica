package org.agil;

import junit.framework.Assert;

import org.agil.Agenda;
import org.agil.Persona;
import org.jbehave.core.annotations.*;
import org.jbehave.core.embedder.Embedder;

public class AsignarVariasDireccionesSteps extends Embedder{
	private static final String MI_DOMICILIO = "Mi domicilio, 4";
	private Persona persona;
	
	@Given("una agenda")
	public void inicializaAgenda(){
		persona = new Persona("nombre 1");
	}
	
	@When("agrego una direccion a una persona")
	public void agregaDireccion(){
		Agenda.agregarDireccion(persona, MI_DOMICILIO);
	}
	
	@Then("compruebo que la direccion queda asignada a dicha persona")
	public void theSystemShouldReturn(){
		Assert.assertEquals(MI_DOMICILIO, Agenda.obtenerDireccion(persona));
	}
	
	
}
